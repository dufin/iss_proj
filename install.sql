DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(60) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` ( `id` , `username` , `password` , `name` ) 
VALUES ( 1, 'admin', '$2a$07$kbbvm061evutz13pwpf8euTU.ws5aCEJwM.qosTqHwfXL.hQIMXqu', 'Administrátor' );


DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id_alba` int(10) AUTO_INCREMENT PRIMARY KEY,
  `nazev` varchar(20),
  `vydavatel` varchar(20),
  `rok_vydani` int(4),
  `id_interpreta` int(11)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `interpret`;
CREATE TABLE `interpret` (
  `id_interpreta` int(11) AUTO_INCREMENT PRIMARY KEY,
  `nazev` varchar(40)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `nosic`;
CREATE TABLE `nosic` (
  `id_nosice` int(10) AUTO_INCREMENT PRIMARY KEY,
  `typ` varchar(10),
  `id_vypujcky` int(11) ,
  `id_alba` int(11) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `skladba`;
CREATE TABLE `skladba` (
  `id_skladba` int(11) AUTO_INCREMENT PRIMARY KEY,
  `nazev` varchar(20),
  `id_alba` int(10) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `vypujcka`;
CREATE TABLE `vypujcka` (
  `id_vypujcky` int(10) AUTO_INCREMENT PRIMARY KEY,
  `termin_od` datetime,
  `termin_do` datetime,
  `id` int(10) UNSIGNED,
  `id_zakaznik` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `zakaznik`;
CREATE TABLE `zakaznik` (
  `id_zakaznik` int(10) AUTO_INCREMENT PRIMARY KEY,
  `jmeno` varchar(40),
  `prijmeni` varchar(40),
  `adresa_ulice` varchar(40),
  `adresa_cislo` int(11),
  `adresa_mesto` varchar(40),
  `adresa_psc` int(5),
  `telefon` int(9),
  `email` varchar(50)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE  nosic ADD FOREIGN KEY(id_alba) REFERENCES album(id_alba);
ALTER TABLE  vypujcka ADD FOREIGN KEY(id) REFERENCES user(id);
ALTER TABLE  vypujcka ADD FOREIGN KEY(id_zakaznik) REFERENCES zakaznik(id_zakaznik);
ALTER TABLE  nosic ADD FOREIGN KEY(id_vypujcky) REFERENCES vypujcka(id_vypujcky);
ALTER TABLE  skladba ADD FOREIGN KEY(id_alba) REFERENCES album(id_alba);
ALTER TABLE  album ADD FOREIGN KEY(id_interpreta) REFERENCES interpret(id_interpreta);

