<?php

namespace Todo;

use Nette;

/**
 * Tabulka vypujcka
 */
class NosicRepository extends Repository {

    public function addNosicVypujcka($idVypujcka, $idNosic) {
        return $this->findBy(array('id_nosice' => $idNosic))->update(array('id_vypujcky' => $idVypujcka));
    }

    public function deleteVypujcka($idVypujcka) {
        return $this->findBy(array('id_vypujcky' => $idVypujcka))->update(array('id_vypujcky' => NULL));
    }

    public function createNosic($typ, $idAlba) {
        return $this->getTable()->insert(array(
                    'id_nosice' => NULL,
                    'typ' => $typ,
                    'id_vypujcky' => NULL,
                    'id_alba' => $idAlba));
    }

    public function findUnborow() {
        return $this->findAll();
    }

}
