<?php

namespace Todo;

use Nette;

/**
 * Tabulka vypujcka
 */
class InterpretRepository extends Repository {

    public function createInterpret($nazev ) {
        return $this->getTable()->insert(array(
        'id_interpreta' => NULL,
        'nazev' =>$nazev));
    }

}