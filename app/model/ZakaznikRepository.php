<?php

namespace Todo;

use Nette;

/**
 * Tabulka vypujcka
 */
class ZakaznikRepository extends Repository {

    public function createZakaznik($jmeno, $prijmeni, $ulice, $cilo, $mesto, $psc, $telefon, $email ) {
        return $this->getTable()->insert(array(
        'id_zakaznik' => NULL,
        'jmeno' =>$jmeno,
        'prijmeni' =>$prijmeni,
        'adresa_ulice' =>$ulice,
        'adresa_cislo' =>$cilo,
        'adresa_mesto' =>$mesto,
        'adresa_psc' =>$psc,
        'telefon' =>$telefon,
        'email' =>$email));
    }
    
    public function findById($id) {
        return $this->findAll()->where('id_zakaznik', $id)->fetch();
    }

}
