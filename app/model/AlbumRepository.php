<?php

namespace Todo;

use Nette;

/**
 * Tabulka vypujcka
 */
class AlbumRepository extends Repository {

    public function createAlbum($nazev, $vydavatel, $rok_vyrani, $id_interpreta) {
        return $this->getTable()->insert(array(
        'id_alba' => NULL,
        'nazev' =>$nazev,
        'vydavatel' =>$vydavatel,
        'rok_vydani' =>$rok_vyrani,
        'id_interpreta' =>$id_interpreta));
    }
    
    

}
