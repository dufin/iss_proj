<?php

namespace Todo;

use Nette;



/**
 * Tabulka vypujcka
 */
class VypujckaRepository extends Repository {

    private $userRepository;
    
    public function createVypujcka($dateTo ,$idVypujcky, $idZamestnance, $idZakaznika) {
        return $this->getTable()->insert(array(
                    'id_vypujcky' => $idVypujcky,
                    'id' => $idZamestnance,
                    'termin_od' => new \DateTime(),
                    'termin_do' => $dateTo,
                    'id_zakaznik' => $idZakaznika,
            
        ));
    }
    
     public function findNewId(){
         return ($this->getTable()->max('id_vypujcky'))+1;
         
     }
     
     public function findAllNames($userRepository, $zakaznikRepository) {
         //$this->userRepository = $userRepository;
         
         $promena = $this->getTable()->order('termin_do');
         
         foreach ($promena as $radek) {
            $radek->termin_od=$radek->termin_od->format('Y-m-d');
            $radek->termin_do=$radek->termin_do->format('Y-m-d');
            $radek->id = $userRepository->findById($radek->id)->username;
            $radek->id_zakaznik = $zakaznikRepository->findById($radek->id_zakaznik)->prijmeni . ' ' . $zakaznikRepository->findById($radek->id_zakaznik)->jmeno ;
          //  $radek->id_zakaznik = $zakaznikRepository->findById($radek->id_zakaznik)->prijmeni;
            
         }         
         return ($promena);
     }

}
