<?php

namespace Todo;

/**
 * Tabulka user
 */
class UserRepository extends Repository {

    public function findByName($username) {
        return $this->findAll()->where('username', $username)->fetch();
    }

    public function findById($id) {
        return $this->findAll()->where('id', $id)->fetch();
    }
    
    public function editUser($userid, $username, $name, $role) {
        $this->getTable()->where(array('id' => $userid))->update(array(
            'username' => $username,
            'name' => $name,
            'role' => $role,
        ));
    }

    public function createUser($username, $password, $name, $role) {

        return $this->getTable()->insert(array(
                    'id' => NULL,
                    'password' => $password,
                    'username' => $username,
                    'name' => $name,
                    'role' => $role,
        ));
    }

}
