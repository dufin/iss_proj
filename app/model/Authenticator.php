<?php

class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator {

    /** @var Nette\Database\Connection */
    private $database;
    private $userRepository;

    public function __construct(Todo\UserRepository $repository) {
        $this->userRepository = $repository;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
        public function authenticate(array $credentials) {
        list($username, $password) = $credentials;
        $row = $this->userRepository->findByName($username);


        if (!$row) {
            throw new Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        }

        if ($row->password !== $this->calculateHash($password, $row->password)) {
            throw new Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
        }

        $arr = $row->toArray();
        unset($arr['password']);
        return new Nette\Security\Identity($row->id, $row->role, $arr);
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public static function calculateHash($password, $salt = null) {
        if ($salt === null) {
            $salt = '$2a$07$' . Nette\Utils\Strings::random(22);
        }
        return crypt($password, $salt);
    }

    public function findByName($username) {
        return $this->findAll()->where('username', $username)->fetch();
    }

    public function setPassword($id, $password) {
        $this->userRepository->findBy(array('id' => $id))->update(array(
            'password' => $this->calculateHash($password),
        ));
    }

}
