<?php

use Nette\Application\UI\Form;

/**
 * Presenter, který zajišťuje výpis seznamů úkolů.
 */
class TaskPresenter extends BasePresenter {

    private $listRepository;
    private $taskRepository;
    private $list;
    private $userRepository;

    public function renderDefault() {
        $this->template->list = $this->list;
    }

    public function inject(Todo\TaskRepository $taskRepository, Todo\UserRepository $userRepository, Todo\ListRepository $listRepository) {
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
        $this->listRepository = $listRepository;
    }

    protected function startup() {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    // @var \Nette\Database\Table\ActiveRow
    public function actionDefault($id) {
        $this->list = $this->listRepository->findBy(array('id' => $id))->fetch();
        if ($this->list === FALSE) {
            $this->setView('notFound');
        }
    }

    protected function createComponentTaskForm() {
        $userPairs = $this->userRepository->findAll()->fetchPairs('id', 'name');
        
        $form = new Form($this, 'taskForm');
        $form->addText('text', 'Úkol:', 40, 100)
                ->addRule(Form::FILLED, 'Je nutné zadat text úkolu.');
        $form->addSelect('userId', 'Pro:', $userPairs)
                ->setPrompt('- Vyberte -')
                ->addRule(Form::FILLED, 'Je nutné vybrat, komu je úkol přiřazen.')
                ->setDefaultValue($this->getUser()->getId());
        $form->addSubmit('create', 'Vytvořit');
        $form->onSuccess[] = $this->taskFormSubmitted;
        return $form;
    }

    public function taskFormSubmitted(Form $form) {
        $this->taskRepository->createTask($this->list->id, $form->values->text, $form->values->userId);
        $this->flashMessage('Úkol přidán.', 'success');
        $this->redirect('this');
    }

    protected function createComponentTaskList() {
        return new Todo\TaskListControl($this->listRepository->tasksOf($this->list), $this->taskRepository);
    }

}
