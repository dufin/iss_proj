<?php

use Nette\Application\UI\Form;

/**
 * 
 */
class NosicPresenter extends BasePresenter {

    private $albumRepository;
    private $interpretRepository;
    private $nosicRepository;

    protected function startup() {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function inject(Todo\AlbumRepository $albumRepository,  Todo\NosicRepository $nosicRepository, Todo\InterpretRepository $interpretRepository) {

        $this->albumRepository = $albumRepository;
        $this->interpretRepository = $interpretRepository;
        $this->nosicRepository = $nosicRepository;
    }

    protected function albumNames() {
        $album = $this->albumRepository->findAll()->fetchPairs('id_alba', 'nazev');
        /*
          $interpret = $this->zakaznikRepository->findAll()->fetchPairs('id_interpret', 'nazev');

          foreach ($album as $index => $hodnota) {

          $album[$index] = $album[$index] . " " . $interpret[$index] . "   ID: " . $zakaznikId[$index];          // do prvku pole uložíme hodnotu 100
          }
         */
        return $album;
    }

    protected function createComponentNewNosicForm() {

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        
        $form = new Form($this, 'newNosicForm');
        //vybrani zamestnance

        $form->addText('typ', 'Typ:')
                ->addRule(Form::FILLED, 'Položka je povinná.');

        $form->addSelect('albumId', 'Interpret:', $this->albumNames())
                ->setPrompt('- Vyberte Album -')
                ->addRule(Form::FILLED, 'Je nutné vybrat album.');

        $form->addSubmit('create', 'Vytvořit');

        $form->onSuccess[] = $this->newNosicFormSubmitted;
        return $form;
    }

    public function newNosicFormSubmitted(Form $form) {
                              //createNosic($typ, $idAlba )
        $this->nosicRepository->createNosic($form->values->typ, $form->values->albumId);
        $this->flashMessage('Nosič vytvořen.', 'success');
        $this->redirect('this');
    }

}
