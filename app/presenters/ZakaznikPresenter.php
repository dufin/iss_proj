<?php

use Nette\Application\UI\Form;

/**
 * 
 */
class ZakaznikPresenter extends BasePresenter {

    private $zakaznikRepository;

    protected function startup() {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function inject(Todo\ZakaznikRepository $zakaznikRepository) {

        $this->zakaznikRepository = $zakaznikRepository;

    }

    protected function createComponentNewZakaznikForm() {

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $form = new Form($this, 'newZakaznikForm');
        //vybrani zamestnance

        $form->addText('zakaznikJmeno', 'Jméno:')
                ->addRule(Form::FILLED, 'Je nutné vyplnit jméno.');

        $form->addText('zakaznikPrijmeni', 'Příjmení:')
                ->addRule(Form::FILLED, 'Je nutné vyplnit příjmení.');

        $form->addText('zakaznikTelefon', 'Telefon:')
                ->addRule(Form::FILLED, 'Je nutné vyplnit telefon.');

        $form->addText('zakaznikEmail', 'Email:')
                ->addRule(Form::FILLED, 'Je nutné vyplnit e-mail.');

        $form->addText('zakaznikUlice', 'Ulice:');

        $form->addText('zakaznikCP', 'ČP:');

        $form->addText('zakaznikMesto', 'Město:');

        $form->addText('zakaznikPSC', 'PSČ:');

        $form->addSubmit('create', 'Vytvořit');
        // $form->onValidate[] = callback($this, 'validateDateInForm');
        $form->onSuccess[] = $this->newZakaznikFormSubmitted;
        return $form;
    }

    public function newZakaznikFormSubmitted(Form $form) {
        $this->zakaznikRepository->createZakaznik($form->values->zakaznikJmeno, $form->values->zakaznikPrijmeni, $form->values->zakaznikUlice, $form->values->zakaznikCP, $form->values->zakaznikMesto, $form->values->zakaznikPSC, $form->values->zakaznikTelefon, $form->values->zakaznikEmail);
        $this->flashMessage('Zákazník vytvořen.', 'success');
        $this->redirect('this');
    }

}
