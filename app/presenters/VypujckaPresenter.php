<?php

use Nette\Application\UI\Form;

/**
 * 
 */
class VypujckaPresenter extends BasePresenter {

    private $vypujckaRepository;
    private $userRepository;
    private $zakaznikRepository;
    private $nosicRepository;
    private $albumRepository;

    protected function startup() {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function inject(Todo\NosicRepository $nosicRepository, Todo\AlbumRepository $albumRepository, Todo\VypujckaRepository $vypujckaRepository, Todo\ZakaznikRepository $zakaznikRepository, Todo\UserRepository $userRepository) {
        $this->vypujckaRepository = $vypujckaRepository;
        $this->userRepository = $userRepository;
        $this->zakaznikRepository = $zakaznikRepository;
        $this->albumRepository = $albumRepository;
        $this->nosicRepository = $nosicRepository;
    }

    protected function zakaznikNames() {
        $zakaznikPrijmeni = $this->zakaznikRepository->findAll()->fetchPairs('id_zakaznik', 'prijmeni');
        $zakaznikJmeno = $this->zakaznikRepository->findAll()->fetchPairs('id_zakaznik', 'jmeno');
        $zakaznikId = $this->zakaznikRepository->findAll()->fetchPairs('id_zakaznik', 'id_zakaznik');

        foreach ($zakaznikPrijmeni as $index => $hodnota) {

            $zakaznikPrijmeni[$index] = $zakaznikPrijmeni[$index] . " " . $zakaznikJmeno[$index] . "   ID: " . $zakaznikId[$index];          // do prvku pole uložíme hodnotu 100
        }

        return $zakaznikPrijmeni;
    }

    /**
     * 
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentNewVypujckaForm() {

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }

        $userPairs = $this->userRepository->findAll()->fetchPairs('id', 'name');
        $freeNosic = $this->nosicRepository->findAll()->where(array('id_vypujcky' => NULL))->fetchPairs('id_nosice', 'id_nosice');

        $form = new Form($this, 'newVypujckaForm');
        //vybrani zamestnance
        if ($this->getUser()->isInRole('admin')) {
            $form->addSelect('zamestnanecId', 'Zaměstnanec:', $userPairs)
                    ->setPrompt('- Vyberte -')
                    ->addRule(Form::FILLED, 'Je nutné vybrat zaměstnance.')
                    ->setDefaultValue($this->getUser()->getId());
        }

        //vybrani zakaznika
        $form->addSelect('zakaznikId', 'Pro:', $this->zakaznikNames())
                ->setPrompt('- Vyberte zákazníka -')
                ->addRule(Form::FILLED, 'Je nutné vybrat zákazníka.');
        //datum
        $form->addText('dateTo', 'Vypůjčeno do:')
                ->addRule(Form::PATTERN, 'Špatný formát data.', '^[0-9]{4}-((0?[0-9])|(1[012]))-(([0-2]?[0-9])|(3[01]))$');

        //nosič
        $form->addSelect('nosicId', 'Nosič:', $freeNosic)
                ->setPrompt('- Vyberte nosič -')
                ->addRule(Form::FILLED, 'Je nutné vybrat nosič.');

        $form->addSubmit('create', 'Vytvořit');
       
        $form->onSuccess[] = $this->newVypujckaFormSubmitted;
        return $form;
    }

    protected function createComponentDeleteVypujckaForm() {

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        
        $vypujcky = $this->nosicRepository->findAll()->where("id_vypujcky > ?", 0)->fetchPairs('id_vypujcky', 'id_vypujcky');

        $form = new Form($this, 'deleteVypujckaForm');
        
        $form->addSelect('vypujckaId', 'Id výpůjčky:', $vypujcky)
                ->setPrompt('- Vyberte Výpůjčku -')
                ->addRule(Form::FILLED, 'Je nutné vybrat nosič.');


        $form->addSubmit('create', 'Zrušit výpůjčku');

        $form->onSuccess[] = $this->newDeleteFormSubmitted;
        return $form;
    }

    public function newDeleteFormSubmitted(Form $form) {

        $this->nosicRepository->deleteVypujcka($form->values->vypujckaId);

        $this->flashMessage('Výpůjčka zrušena.', 'success');
        $this->redirect('this');
    }

    /**
     * @param \Nette\Application\UI\Form $form
     */
    public function newVypujckaFormSubmitted(Form $form) {
        $idNewVypujcka = $this->vypujckaRepository->findNewId();
        if ($this->getUser()->isInRole('admin')) {
            $this->vypujckaRepository->createVypujcka($form->values->dateTo, $idNewVypujcka, $form->values->zamestnanecId, $form->values->zakaznikId);
        } else {
            $this->vypujckaRepository->createVypujcka($form->values->dateTo, $idNewVypujcka, $this->getUser()->getIdentity()->id, $form->values->zakaznikId);
        }
        $this->nosicRepository->addNosicVypujcka($idNewVypujcka, $form->values->nosicId);
        $this->flashMessage('Výpůjčka uložena.', 'success');
        $this->redirect('this');
    }

    public function renderDefault() {
        $this->template->vypujcky = $this->vypujckaRepository->findAllNames($this->userRepository, $this->zakaznikRepository);
               
        
    }
    
}
