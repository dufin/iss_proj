<?php

use Nette\Application\UI\Form;
use Nette\Security as NS;

/**
 */
class UserPresenter extends BasePresenter {

    /** @var Todo\UserRepository */
    private $userRepository;

    /** @var Todo\Authenticator */
    private $authenticator;
    private $users;

    protected function startup() {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $this->userRepository = $this->context->userRepository;
        $this->authenticator = $this->context->authenticator;
    }

    protected function createComponentPasswordForm() {
        $form = new Form();
        $form->addPassword('oldPassword', 'Staré heslo:', 30)
                ->addRule(Form::FILLED, 'Je nutné zadat staré heslo.');
        $form->addPassword('newPassword', 'Nové heslo:', 30)
                ->addRule(Form::MIN_LENGTH, 'Nové heslo musí mít alespoň %d znaků.', 6);
        $form->addPassword('confirmPassword', 'Potvrzení hesla:', 30)
                ->addRule(Form::FILLED, 'Nové heslo je nutné zadat ještě jednou pro potvrzení.')
                ->addRule(Form::EQUAL, 'Zadná hesla se musejí shodovat.', $form['newPassword']);
        $form->addSubmit('set', 'Změnit heslo');
        $form->onSuccess[] = $this->passwordFormSubmitted;
        return $form;
    }

    public function passwordFormSubmitted(Form $form) {
        $values = $form->getValues();
        $user = $this->getUser();
        try {
            $this->authenticator->authenticate(array($user->getIdentity()->username, $values->oldPassword));
            $this->authenticator->setPassword($user->getId(), $values->newPassword);
            $this->flashMessage('Heslo bylo změněno.', 'success');
            $this->redirect('Homepage:');
        } catch (NS\AuthenticationException $e) {
            $form->addError('Zadané heslo není správné.');
        }
    }

    public function renderDefault() {
        $this->template->users = $this->userRepository->findAll();
    }

    protected function getUsername($id) {
        return $username = $this->userRepository->findBy(array('id' => $id))->fetch()->username;
    }

    protected function getNameUser($id) {
        return $name = $this->userRepository->findBy(array('id' => $id))->fetch()->name;
    }

    protected function getRole($id) {
        return $role = $this->userRepository->findBy(array('id' => $id))->fetch()->role;
    }

    protected function createComponentEditUserForm($id) {

        $roleType = array(
            'admin' => 'admin',
            'user' => 'user',
        );

        $form = new Form($this, 'editUserForm');

        $form->addHidden('userid', $id)
                ->setDefaultValue($id);

        $form->addText('username', 'Uživatelské jméno:')
                ->addRule(Form::FILLED, 'Je nutné zadat text úkolu.')
                ->setDefaultValue($this->getUsername($id));

        $form->addText('name', 'Jméno a příjmení:')
                ->addRule(Form::FILLED, 'Je nutné zadat text úkolu.')
                ->setDefaultValue($this->getNameUser($id));

        $form->addSelect('role', 'Role:', $roleType)
                ->setPrompt('- Vyberte -')
                ->addRule(Form::FILLED, 'Je nutné vybrat zaměstnance.')
                ->setDefaultValue($this->getRole($id));

        $form->addSubmit('update', 'Upravit');

        $form->onSuccess[] = $this->editUserFormSubmitted;
        return $form;
    }

    public function editUserFormSubmitted(Form $form) {
        $this->userRepository->editUser($form->values->userid, $form->values->username, $form->values->name, $form->values->role);
        $this->flashMessage('Uživatel editován.', 'success');
        $this->redirect('User:default');
    }

    public function renderEdit($id) {

        $this->createComponentEditUserForm($id);
    }

    protected function createComponentCreateNewUserForm() {

        $roleType = array(
            'admin' => 'admin',
            'user' => 'user',
        );

        $form = new Form($this, 'createNewUserForm');


        $form->addText('username', 'Uživatelské jméno:')
                ->addRule(Form::FILLED, 'Je nutné zadat uživatelské jméno.');

        $form->addText('password', 'Heslo:')
                ->addRule(Form::FILLED, 'Je nutné zadat heslo.');


        $form->addText('name', 'Jméno a příjmení:')
                ->addRule(Form::FILLED, 'Je nutné zadat jméno a příjmení.');


        $form->addSelect('role', 'Role:', $roleType)
                ->setPrompt('- Vyberte oprávnění -')
                ->addRule(Form::FILLED, 'Je nutné vybrat práva.');


        $form->addSubmit('create', 'Vytvořit');

        $form->onSuccess[] = $this->createNewUserFormSubmitted;
        return $form;
    }

    public function createNewUserFormSubmitted(Form $form) {
        $this->userRepository->createUser($form->values->username, $this->authenticator->calculateHash($form->values->password), $form->values->name, $form->values->role);
        $this->flashMessage('Uživatel Vytvořen.', 'success');
        $this->redirect('User:default');
    }

}
