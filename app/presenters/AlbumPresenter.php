<?php

use Nette\Application\UI\Form;

/**
 * 
 */
class AlbumPresenter extends BasePresenter {

    private $albumRepository;
    private $interpretRepository;

    protected function startup() {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function inject(Todo\AlbumRepository $albumRepository, Todo\InterpretRepository $interpretRepository) {

        $this->albumRepository = $albumRepository;
        $this->interpretRepository = $interpretRepository;
    }

    protected function createComponentNewAlbumForm() {

        $interpreti = $this->interpretRepository->findAll()->order('nazev ASC')->fetchPairs('id_interpreta', 'nazev');

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $form = new Form($this, 'newAlbumForm');
        //vybrani zamestnance

        $form->addText('nazev', 'Název:')
                ->addRule(Form::FILLED, 'Položka je povinná.');

        $form->addText('rokVydani', 'Rok vydání:')
                ->addRule(Form::FILLED, 'Položka je povinná.');

        $form->addText('vydavatel', 'Vydavatel:')
                ->addRule(Form::FILLED, 'Položka je povinná.');

        $form->addSelect('interpretId', 'Interpret:', $interpreti)
                ->setPrompt('- Vyberte interpreta -')
                ->addRule(Form::FILLED, 'Je nutné vybrat interpreta.');

        $form->addSubmit('create', 'Vytvořit');

        $form->onSuccess[] = $this->newAlbumFormSubmitted;
        return $form;
    }

    public function newAlbumFormSubmitted(Form $form) {
        //createAlbum($nazev, $vydavatel, $rok_vyrani, $id_interpreta)        
        $this->albumRepository->createAlbum($form->values->nazev, $form->values->rokVydani, $form->values->vydavatel, $form->values->interpretId);
        $this->flashMessage('Album vytvořeno.', 'success');
        $this->redirect('this');
    }

    protected function createComponentNewInterpretForm() {

        
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $form = new Form($this, 'newInterpretForm');
        //vybrani zamestnance

        $form->addText('nazev', 'Název interpreta:')
                ->addRule(Form::FILLED, 'Položka je povinná.');


        $form->addSubmit('create', 'Vytvořit');

        $form->onSuccess[] = $this->newInterpretFormSubmitted;
        return $form;
    }

    public function newInterpretFormSubmitted(Form $form) {
        //createAlbum($nazev, $vydavatel, $rok_vyrani, $id_interpreta)        
        $this->interpretRepository->createInterpret($form->values->nazev);
        $this->flashMessage('Interpret vytvořen.', 'success');
        $this->redirect('this');
    }

}
