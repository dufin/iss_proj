<?php

use Nette\Application\UI\Form;

/**
 * 
 */
class SkladbaPresenter extends BasePresenter {

    private $albumRepository;
    private $skladbaRepository;

    protected function startup() {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function inject(Todo\AlbumRepository $albumRepository, Todo\SkladbaRepository $skladbaRepository) {

        $this->albumRepository = $albumRepository;
        $this->skladbaRepository = $skladbaRepository;
    }

    protected function createComponentNewSkladbaForm() {

        $alba = $this->albumRepository->findAll()->fetchPairs('id_alba', 'nazev');

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $form = new Form($this, 'newSkladbaForm');
       

        $form->addText('nazev', 'Název:')
                ->addRule(Form::FILLED, 'Položka je povinná.');

        $form->addSelect('albumId', 'Album:', $alba)
                ->setPrompt('- Vyberte album -')
                ->addRule(Form::FILLED, 'Je nutné vybrat interpreta.');

        $form->addSubmit('create', 'Vytvořit');

        $form->onSuccess[] = $this->newSkladbaFormSubmitted;
        return $form;
    }

    public function newSkladbaFormSubmitted(Form $form) {
             
        $this->skladbaRepository->createSkladba($form->values->nazev, $form->values->albumId);
        $this->flashMessage('Skladba vytvořena.', 'success');
        $this->redirect('this');
    }

}
